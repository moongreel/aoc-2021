#[aoc_generator(day7)]
fn parse_input(input: &str) -> Vec<isize> {
    input.split(',')
        .filter_map(|x| x.parse::<isize>().ok())
        .collect()
}

#[aoc(day7, part1)]
fn part1(input: &[isize]) -> isize {
    let minval = *input.iter().min().unwrap();
    let maxval = *input.iter().max().unwrap();
    let mut cost = isize::MAX;

    for dist in minval..maxval {
        let tmp_cost = input.iter().map(|x| (x - dist).abs()).sum();
        if tmp_cost < cost {
            cost = tmp_cost;
        }
    }

    cost
}

#[aoc(day7, part2)]
fn part2(input: &[isize]) -> isize {
    let minval = *input.iter().min().unwrap();
    let maxval = *input.iter().max().unwrap();
    let mut cost = isize::MAX;

    for dist in minval..maxval+1 {
        let tmp_cost = input.iter().map(|x| {
            let y = (x - dist).abs();
            (y*(y + 1))/2
        }).sum();
        if tmp_cost < cost {
            cost = tmp_cost;
        }
    }

    cost
}

#[cfg(test)]
mod test {
    use super::*;

    const INP1: &str = r"16,1,2,0,4,2,7,1,2,14";

    #[test]
    fn test_part1() {
        let inp = parse_input(INP1);
        assert_eq!(part1(&inp), 37);
    }

    #[test]
    fn test_part2() {
        let inp = parse_input(INP1);
        assert_eq!(part2(&inp), 168);
    }
}
