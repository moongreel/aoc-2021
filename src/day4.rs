#[derive(Debug)]
struct Board {
    rows: Vec<Vec<isize>>,
    cols: Vec<Vec<isize>>,
}

impl Board {
    pub fn new(rows: Vec<Vec<isize>>) -> Self {
        let mut cols: Vec<Vec<isize>> = rows.clone();
        for (i, col) in cols.iter_mut().enumerate() {
            for (j, row) in rows.clone().iter().enumerate() {
                col[j] = row[i];
            }
        }

        Self {
            rows,
            cols,
        }
    }

    pub fn is_winner(&self, nums: &[isize]) -> bool {
        if self.rows.iter().any(|row| row.iter().all(|x| nums.contains(x))) {
            return true;
        }

        if self.cols.iter().any(|col| col.iter().all(|x| nums.contains(x))) {
            return true;
        }

        false
    }

    pub fn score(&self, nums: &[isize]) -> isize {
        nums.last().unwrap() * self.rows.iter().flatten()
            .fold(0, |acc, ele| {
                if nums.contains(ele) { acc }
                else { acc + ele }
            })
    }
}

#[aoc_generator(day4)]
fn parse_input(input: &str) -> (Vec<isize>, Vec<Board>) {
    let mut lines = input.lines();
    let nums = lines.next()
        .unwrap()
        .split(',').filter_map(|x| x.parse::<isize>().ok())
        .collect();

    lines.next();

    let mut boards = Vec::new();
    let mut board = Vec::new();
    loop {
        if let Some(line) = lines.next() {
            let row: Vec<isize> = line.split(' ').filter_map(|x| x.parse::<isize>().ok()).collect();
            if row.len() == 5 {
                board.push(row);
            } else {
                boards.push(Board::new(board));
                board = Vec::new();
            }
        } else {
            if board.len() == 5 {
                boards.push(Board::new(board));
            }
            break;
        }
    }

    (nums, boards)
}

#[aoc(day4, part1)]
fn part1(input: &(Vec<isize>, Vec<Board>)) -> isize {
    let (nums, boards) = input;

    for i in 0..nums.len() {
        for board in boards {
            if board.is_winner(&nums[..i]) {
                return board.score(&nums[..i]);
            }
        }
    }

    panic!("No winners!");
}

#[aoc(day4, part2)]
fn part2(input: &(Vec<isize>, Vec<Board>)) -> isize {
    let (nums, boards) = input;
    let mut winners: Vec<usize> = Vec::new();

    for i in 0..nums.len() {
        for (idx, board) in boards.iter().enumerate() {
            if board.is_winner(&nums[..i]) && !winners.contains(&idx) {
                winners.push(idx); 
            }
        }
    }

    let widx = *winners.last().unwrap();
    for i in 0..nums.len() {
        if boards[widx].is_winner(&nums[..i]) {
            return boards[widx].score(&nums[..i]);
        }
    }

    panic!("No winners!");
}

#[cfg(test)]
mod test {
    use super::*;

    const INP1: &str = r"7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7";

    #[test]
    fn test_part1() {
        let inp = parse_input(INP1);
        assert_eq!(part1(&inp), 4512);
    }

    #[test]
    fn test_part2() {
        let inp = parse_input(INP1);
        assert_eq!(part2(&inp), 1924);
    }
}
