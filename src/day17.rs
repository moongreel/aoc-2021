use std::ops::Range;

struct Probe {
    pub pos: (isize, isize),
    pub vel: (isize, isize),
}

impl Probe {
    pub fn new(vel: (isize, isize)) -> Self {
        Probe { pos: (0, 0), vel }
    }

    pub fn step(&mut self) -> (isize, isize) {
        self.pos.0 += self.vel.0;
        self.pos.1 += self.vel.1;

        self.vel.0 -= 1;
        self.vel.1 -= 1;
        if self.vel.0 < 0 {
            self.vel.0 += 1;
        }
        self.pos
    }
}

#[aoc_generator(day17)]
fn parse_input(input: &str) -> (Range<isize>, Range<isize>) {
    let (_, ranges) = input.split_once(':').unwrap();
    let (xrange, yrange) = ranges.split_once(',').unwrap();
    let (_, xrange) = xrange.split_once('=').unwrap();
    let (_, yrange) = yrange.split_once('=').unwrap();
    let (xstart, xend) = xrange.split_once("..").unwrap();
    let (ystart, yend) = yrange.split_once("..").unwrap();

    (xstart.parse::<isize>().unwrap()..xend.parse::<isize>().unwrap()+1,
     ystart.parse::<isize>().unwrap()..yend.parse::<isize>().unwrap()+1)
}

#[aoc(day17, part1)]
fn part1(input: &(Range<isize>, Range<isize>)) -> isize {
    let (xrange, yrange) = input;
    let mut ymax = 0;

    for xvel in 0..xrange.end {
        for yvel in 0..100 {
            let mut probe = Probe::new((xvel, yvel));
            let mut tmp_ymax = 0;
            loop {
                probe.step();
                tmp_ymax = tmp_ymax.max(probe.pos.1);
                if xrange.contains(&probe.pos.0) && yrange.contains(&probe.pos.1) {
                    ymax = ymax.max(tmp_ymax);
                    break;
                }

                if probe.pos.0 > xrange.end || probe.pos.1 < yrange.end {
                    break;
                }
            }
        }
    }

    ymax
}

#[aoc(day17, part2)]
fn part2(input: &(Range<isize>, Range<isize>)) -> isize {
    let (xrange, yrange) = input;
    let mut cnt = 0;

    for xvel in 0..=xrange.end {
        for yvel in yrange.start..yrange.start.abs() {
            let mut probe = Probe::new((xvel, yvel));
            loop {
                probe.step();
                if xrange.contains(&probe.pos.0) && yrange.contains(&probe.pos.1) {
                    cnt += 1;
                    break;
                }

                if probe.pos.0 > xrange.end || probe.pos.1 < yrange.start {
                    break;
                }
            }
        }
    }

    cnt
}

#[cfg(test)]
mod test {
    use super::*;

    const INP1: &str = r"target area: x=20..30, y=-10..-5";

    #[test]
    fn test_part1() {
        let inp = parse_input(INP1);
        assert_eq!(part1(&inp), 45);
    }

    #[test]
    fn test_part2() {
        let inp = parse_input(INP1);
        assert_eq!(part2(&inp), 112);
    }
}
