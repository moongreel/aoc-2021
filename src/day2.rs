enum Dir {
    Up,
    Down,
    Forward,
}

#[aoc_generator(day2)]
fn parse_input(input: &str) -> Vec<(Dir, isize)> {
    input
        .lines()
        .filter_map(|x| x.split_once(" "))
        .map(|(x, y)| {
            let n = y.parse::<isize>().unwrap();
            let d = match x {
                "up" => Dir::Up,
                "down" => Dir::Down,
                "forward" => Dir::Forward,
                _ => panic!("{}", x),
            };

            (d, n)
        })
        .collect()
}

#[aoc(day2, part1)]
fn part1(input: &[(Dir, isize)]) -> isize {
    input.iter()
        .fold([0;2], |[depth, horiz], (dir, amt)| {
            match dir {
                Dir::Up => [depth - amt, horiz],
                Dir::Down => [depth + amt, horiz],
                Dir::Forward => [depth, horiz + amt],
            }
        })
        .iter()
        .product()
}

#[aoc(day2, part2)]
fn part2(input: &[(Dir, isize)]) -> isize {
    input.iter()
        .fold([0;3], |[aim, horiz, depth], (dir, amt)| {
            match dir {
                Dir::Up => [aim - amt, horiz, depth],
                Dir::Down => [aim + amt, horiz, depth],
                Dir::Forward => [aim, horiz + amt, depth + amt * aim],
            }
        })[1..3]
        .iter()
        .product()
}

#[cfg(test)]
mod test {
    use super::*;

    const INP1: &str = r"forward 5
down 5
forward 8
up 3
down 8
forward 2";

    #[test]
    fn test_part1() {
        let inp = parse_input(INP1);
        assert_eq!(part1(&inp), 150);
    }

    #[test]
    fn test_part2() {
        let inp = parse_input(INP1);
        assert_eq!(part2(&inp), 900);
    }
}
