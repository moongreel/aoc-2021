#[aoc_generator(day15)]
fn parse_input(input: &str) -> Vec<Vec<isize>> {
    input
        .lines()
        .map(|x| x.chars().map(|y| y.to_digit(10).unwrap() as isize).collect())
        .collect()
}

fn solve(input: &[Vec<isize>]) -> isize {
    let mut output = input.to_vec();
    output.iter_mut().flatten().for_each(|x| *x=0);
    output[1][0] = input[1][0];
    output[0][1] = input[0][1];

    let btmy = input.len()-1;
    let btmx = input[0].len()-1;

    loop {
        let mut end = true;
        let mut tmp_output = output.clone();
        for (j, row) in output.iter().enumerate() {
            for (i, out) in row.iter().enumerate() {
                if *out == 0 || (i == btmx && j == btmy) {
                    continue;
                }

                let ii = i as isize;
                let jj = j as isize;

                for (x, y) in [(1, 0), (0, 1), (-1, 0), (0, -1)] {
                    let x = (ii+x) as usize;
                    let y = (jj+y) as usize;
                    if let Some(row) = input.get(y) {
                        if let Some(val) = row.get(x) {
                            let vout = &mut tmp_output[y][x];
                            if *vout == 0 || val + out < *vout {
                                *vout = val + out;
                                end = false;
                            }
                        }
                    }
                }
            }
        }
        output = tmp_output;
        if end { break; }
    }

    output[btmy][btmx]
}

#[aoc(day15, part1)]
fn part1(input: &[Vec<isize>]) -> isize {
    solve(input)
}

#[aoc(day15, part2)]
fn part2(input: &[Vec<isize>]) -> isize {
    let new_input = (0..5).into_iter().map(|ytile| {
        input.iter().map(|row| {
            (0..5).into_iter().map(|xtile| {
                row.iter().map(|val| (val - 1 + xtile + ytile) % 9 + 1).collect::<Vec<_>>()
            }).flatten().collect::<Vec<_>>()
        }).collect::<Vec<_>>()
    }).flatten().collect::<Vec<_>>();

    solve(&new_input)
}

#[cfg(test)]
mod test {
    use super::*;

    const INP1: &str = r"1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581";

    const INP2: &str = r"19111
11191
99991
99991
99911
99919
99911";

    #[test]
    fn test_part1() {
        let inp = parse_input(INP1);
        assert_eq!(part1(&inp), 40);
    }

    #[test]
    fn test_part1_2() {
        let inp = parse_input(INP2);
        assert_eq!(part1(&inp), 14);
    }

    #[test]
    fn test_part2() {
        let inp = parse_input(INP1);
        assert_eq!(part2(&inp), 315);
    }
}
