#[aoc_generator(day10)]
fn parse_input(input: &str) -> Vec<String> {
    input
        .lines()
        .map(|x| x.to_string())
        .collect()
}

#[aoc(day10, part1)]
fn part1(input: &[String]) -> isize {
    input.iter().fold(0, |score, line| {
        let mut state = Vec::new();
        for c in line.chars() {
            match c {
                '('|'['|'{'|'<' => state.push(c),
                ')' => if state.pop() != Some('(') { return score + 3; },
                ']' => if state.pop() != Some('[') { return score + 57; },
                '}' => if state.pop() != Some('{') { return score + 1197; },
                '>' => if state.pop() != Some('<') { return score + 25137; },
                _ => {},
            }
        }
        score
    })
}

#[aoc(day10, part2)]
fn part2(input: &[String]) -> isize {
    let mut scores: Vec<isize> = input.iter().filter_map(|line| {
        let mut state = Vec::new();
        let mut corrupted = false;
        for c in line.chars() {
            match c {
                '('|'['|'{'|'<' => state.push(c),
                ')' => if state.pop() != Some('(') { corrupted=true; break; }
                ']' => if state.pop() != Some('[') { corrupted=true; break; }
                '}' => if state.pop() != Some('{') { corrupted=true; break; }
                '>' => if state.pop() != Some('<') { corrupted=true; break; }
                _ => {},
            }
        }

        if !corrupted {
            state.reverse();
            Some(state.iter().fold(0, |acc, s| {
                let scr = match s {
                    '(' => 1,
                    '[' => 2,
                    '{' => 3,
                    '<' => 4,
                    _ => 0,
                };
                acc * 5 + scr
            }))
        } else {
            None
        }
    }).collect();

    scores.sort_unstable();
    scores[scores.len() / 2]
}

#[cfg(test)]
mod test {
    use super::*;

    const INP1: &str = r"[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]";

    #[test]
    fn test_part1() {
        let inp = parse_input(INP1);
        assert_eq!(part1(&inp), 26397);
    }

    #[test]
    fn test_part2() {
        let inp = parse_input(INP1);
        assert_eq!(part2(&inp), 288957);
    }
}
