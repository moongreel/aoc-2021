#[aoc_generator(day3)]
fn parse_input(input: &str) -> (usize, Vec<usize>) {
    let nums = input
        .lines()
        .filter_map(|x| usize::from_str_radix(x, 2).ok())
        .collect();
    let len = input.lines().next().unwrap().len();
    (len, nums)
}

fn gamma(input: &[usize], len: usize) -> Vec<usize> {
    input.iter()
        .fold(vec![0;len], |mut acc: Vec<usize>, ele| {
            for (bit, count) in acc.iter_mut().enumerate() {
                *count += (ele >> (len - bit - 1)) & 1;
            }
            acc
        }).iter().map(|x| {
            if (*x > input.len() / 2) ||
               (*x == input.len() / 2 && input.len() % 2 == 0) { 1 }
            else { 0 }
        }).collect::<Vec<usize>>()
}

fn epsilon(input: &[usize], len: usize) -> Vec<usize> {
    gamma(input, len).iter().map(|x| (!x) & 1).collect()
}

fn bin_to_num(input: &[usize]) -> usize {
    usize::from_str_radix(
        input.iter().map(|x| format!("{}", x)).collect::<String>().as_ref(),
        2
    ).unwrap()
}

#[aoc(day3, part1)]
fn part1(input: &(usize, Vec<usize>)) -> usize {
    let (len, input) = input;
    let gamma = gamma(input, *len);
    let epsilon = epsilon(input, *len);

    bin_to_num(&gamma) * bin_to_num(&epsilon)
}

#[aoc(day3, part2)]
fn part2(input: &(usize, Vec<usize>)) -> usize {
    let (len, input) = input;
    let len = *len;

    let mut o2_rating = input.clone();
    let mut co2_rating = input.clone();

    for bit in 0..len {
        if o2_rating.len() == 1 {
            break;
        }

        let mut tmp_o2_rating = Vec::new();
        let gamma = gamma(&o2_rating, len);

        for num in o2_rating {
            let flag = (num & (1 << (len - bit - 1))) >> (len - bit - 1);
            if flag == gamma[bit] {
                tmp_o2_rating.push(num);
            }
        }

        o2_rating = tmp_o2_rating.clone();
    }

    for bit in 0..len {
        if co2_rating.len() == 1 {
            break;
        }

        let mut tmp_co2_rating = Vec::new();
        let epsilon = epsilon(&co2_rating, len);

        for num in co2_rating {
            let flag = (num & (1 << (len - bit - 1))) >> (len - bit - 1);
            if flag == epsilon[bit] {
                tmp_co2_rating.push(num);
            }
        }

        co2_rating = tmp_co2_rating.clone();
    }

    o2_rating[0] * co2_rating[0]
}

#[cfg(test)]
mod test {
    use super::*;

    const INP1: &str = r"00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010";

    #[test]
    fn test_part1() {
        let inp = parse_input(INP1);
        assert_eq!(part1(&inp), 198);
    }

    #[test]
    fn test_part2() {
        let inp = parse_input(INP1);
        assert_eq!(part2(&inp), 230);
    }
}
