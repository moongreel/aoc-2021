#[aoc_generator(day11)]
fn parse_input(input: &str) -> Vec<Vec<isize>> {
    input
        .lines()
        .map(|x| x.chars().filter_map(|y| y.to_digit(10)).map(|y| y as isize).collect())
        .collect()
}

fn step(input: &mut Vec<Vec<isize>>) -> usize {
    let mut flashes = 0;
    input.iter_mut().flatten().for_each(|x| *x += 1);
    loop {
        let mut flash = false;
        input.iter_mut().flatten().for_each(|x| if *x > 9 { 
            *x = 0;
            flash = true;
            flashes += 1;
        });
        if !flash {
            break;
        }
        let mut output = input.clone();
        for (j, row) in input.iter().enumerate() {
            for (i, val) in row.iter().enumerate() {
                if *val <= 0 {
                    continue;
                }

                let adj = [
                    input.get((j as isize - 1) as usize),
                    input.get(j),
                    input.get(j + 1),
                ];

                let power = adj.iter().map(|r| {
                    if r.is_none() { [None, None, None] }
                    else {
                        let r = r.unwrap();
                        [r.get((i as isize - 1) as usize), r.get(i), r.get(i+1)]
                    }
                }).flatten().filter(|x| *x == Some(&0)).count();

                output[j][i] += power as isize;
            }
        }
        output.iter_mut().flatten().for_each(|x| if *x == 0 { *x = -1 });
        *input = output;
    }
    input.iter_mut().flatten().for_each(|x| if *x < 0 { *x = 0 });
    flashes
}

#[aoc(day11, part1)]
fn part1(input: &[Vec<isize>]) -> usize {
    let mut input = input.to_vec();
    (0..100).into_iter().map(|_| step(&mut input)).sum()
}

#[aoc(day11, part2)]
fn part2(input: &[Vec<isize>]) -> usize {
    let mut input = input.to_vec();
    let total = input.len() * input[0].len();
    let mut s = 0;
    loop {
        s += 1;
        if step(&mut input) == total {
            return s;
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    const INP1: &str = r"5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526";

    #[test]
    fn test_part1() {
        let inp = parse_input(INP1);
        assert_eq!(part1(&inp), 1656);
    }

    #[test]
    fn test_part2() {
        let inp = parse_input(INP1);
        assert_eq!(part2(&inp), 195);
    }
}
