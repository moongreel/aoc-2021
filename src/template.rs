use std::collections::{
    HashMap,
    HashSet,
};

use itertools::Itertools;
use regex::Regex;

#[aoc_generator(day<DAY>)]
fn parse_input(input: &str) -> Vec<usize> {
    input
        .lines()
        .filter_map(|x| x.parse::<usize>().ok())
        .collect()
}

#[aoc(day<DAY>, part1)]
fn part1(input: &[usize]) -> usize {
    panic!();
}

#[aoc(day<DAY>, part2)]
fn part2(input: &[usize]) -> usize {
    panic!();
}

#[cfg(test)]
mod test {
    use super::*;

    const INP1: &str = r"";

    #[test]
    fn test_part1() {
        let inp = parse_input(INP1);
        assert_eq!(part1(&inp), 1);
    }

    #[test]
    fn test_part2() {
        let inp = parse_input(INP1);
        assert_eq!(part2(&inp), 1);
    }
}
