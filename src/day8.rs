use std::collections::HashMap;

struct Signal {
    pub set: Vec<char>
}

impl Signal {
    pub fn new(setting: &str) -> Self {
        Self { set: setting.chars().collect() }
    }

    pub fn is_unique(&self) -> bool {
        matches!(self.set.len(), 2|3|4|7)
    }

    pub fn to_bits(&self) -> u8 {
        let mut res = 0;
        for c in self.set.iter() {
            match c {
                'a' => res |= 1 << 6,
                'b' => res |= 1 << 5,
                'c' => res |= 1 << 4,
                'd' => res |= 1 << 3,
                'e' => res |= 1 << 2,
                'f' => res |= 1 << 1,
                'g' => res |= 1 << 0,
                _ => {}
            }
        }
        res
    }

    pub fn to_num(&self) -> usize {
        let bits = self.to_bits();
        match bits {
            0b1110111 => 0,
            0b0010010 => 1,
            0b1011101 => 2,
            0b1011011 => 3,
            0b0111010 => 4,
            0b1101011 => 5,
            0b1101111 => 6,
            0b1010010 => 7,
            0b1111111 => 8,
            0b1111011 => 9,
            _ => panic!(),
        }
    }
}

fn make_mapping(map: [u8;7]) -> HashMap<char,char> {
    let chars: Vec<char> = "abcdefg".chars().collect();
    let mut mapping: HashMap<char, char> = HashMap::new();

    fn get_bit(mut num: u8) -> usize {
        let mut cnt = 0;
        loop {
            if num == 1 {
                return 6 - cnt;
            }
            cnt += 1;
            num >>= 1;
        }
    }

    for (x, y) in map.into_iter().zip("abcdefg".chars().into_iter()) {
        mapping.insert(chars[get_bit(x)], y);
    }

    mapping
}

#[aoc_generator(day8)]
fn parse_input(input: &str) -> Vec<(Vec<Signal>, Vec<Signal>)> {
    input
        .lines()
        .map(|x| {
            let (left, right) = x.split_once(" | ").unwrap();
            let signals = left.split(' ').map(Signal::new).collect();
            let outputs = right.split(' ').map(Signal::new).collect();

            (signals, outputs)
        })
        .collect()
}

#[aoc(day8, part1)]
fn part1(input: &[(Vec<Signal>, Vec<Signal>)]) -> usize {
    input.iter()
        .map(|(_, out)| out.iter().filter(|x| x.is_unique()).count())
        .sum()
}

#[aoc(day8, part2)]
fn part2(input: &[(Vec<Signal>, Vec<Signal>)]) -> usize {
    input.iter().map(|(signals, output)| {
        let mut num_069 = 0;
        let mut num_235 = 0;
        let mut num_a235 = 0x7f;
        let mut num_4 = 0;
        let mut num_7 = 0;
        let mut num_1 = 0;

        for sig in signals {
            match sig.set.len() {
                2 => num_1 ^= sig.to_bits(),
                3 => num_7 ^= sig.to_bits(),
                4 => num_4 ^= sig.to_bits(),
                5 => {
                    num_235 ^= sig.to_bits();
                    num_a235 &= sig.to_bits();
                }
                6 => num_069 ^= sig.to_bits(),
                _ => {}
            }
        }

        let top = num_7 ^ num_1;
        let lbot = (!(num_a235 | num_4)) & 0x7f;
        let bot = (!(num_4 | num_7 | lbot)) & 0x7f;
        let rtop = (!(num_069|num_235))&0x7f;
        let rbot = num_1 ^ rtop;
        let mid = num_a235 ^ bot ^ top;
        let ltop = (!(top | lbot | bot | rtop | rbot | mid))&0x7f;

        let mapping = make_mapping([top, ltop, rtop, mid, lbot, rbot, bot]);

        let nums: Vec<usize> = output.iter().map(|out| {
            let s: String = out.set.iter().map(|x| *mapping.get(x).unwrap()).collect();
            Signal::new(&s).to_num()
        }).collect();

        nums[0] * 1000 + nums[1] * 100 + nums[2] * 10 + nums[3]
    }).sum()
}

#[cfg(test)]
mod test {
    use super::*;

    const INP1: &str = r"be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce";

    #[test]
    fn test_part1() {
        let inp = parse_input(INP1);
        assert_eq!(part1(&inp), 26);
    }

    #[test]
    fn test_part2() {
        let inp = parse_input(INP1);
        assert_eq!(part2(&inp), 61229);
    }
}
