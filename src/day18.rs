use itertools::Itertools;

#[derive(Debug, Clone, Copy)]
enum Node {
    Open,
    Close,
    Sep,
    Num(isize),
}

fn parse_line(input: &str) -> Vec<Node> {
    input.chars().map(|x| {
        match x {
            '[' => Node::Open,
            ']' => Node::Close,
            ',' => Node::Sep,
            n => Node::Num(n.to_digit(10).unwrap() as isize),
        }
    }).collect()
}

fn explode(nodes: &mut Vec<Node>, idx: usize) {
    let mut left = 0;
    let mut right = 0;
    let start = idx;
    let mut end = idx;
    let mut isleft = true;

    for (i, n) in nodes[start..].iter().enumerate() {
        match n {
            Node::Open => {
                isleft = true;
            },

            Node::Close => {
                end = i+start;
                break;
            },

            Node::Sep => isleft = false,

            Node::Num(num) => {
                match isleft {
                    true => left = *num,
                    _ => right = *num,
                }
            }
        }
    }

    for i in (0..=start).rev() {
        if let Some(Node::Num(n)) = nodes.get_mut(i) {
            *n += left;
            break;
        }
    }

    for i in end..=nodes.len()-1 {
        if let Some(Node::Num(n)) = nodes.get_mut(i) {
            *n += right;
            break;
        }
    }

    for _ in 0..=end-start {
        nodes.remove(start);
    }
    nodes.insert(start, Node::Num(0));
}

fn split(nodes: &mut Vec<Node>, idx: usize) {
    if let Some(Node::Num(val)) = nodes.clone().get(idx) {
        nodes.remove(idx);
        nodes.insert(idx, Node::Open);
        nodes.insert(idx+1, Node::Num(val/2));
        nodes.insert(idx+2, Node::Sep);
        nodes.insert(idx+3, Node::Num(val/2 + val%2));
        nodes.insert(idx+4, Node::Close);
    }
}

fn magnitude(nodes: &[Node], start: usize, end: usize) -> isize {
    let mut sep = 0;
    let mut level = 0;

    if let Node::Num(val) = nodes[start] {
        return val;
    }

    for (i, n) in nodes[start..=end].iter().enumerate() {
        match n {
            Node::Open => level += 1,
            Node::Close => level -= 1,
            Node::Sep => {
                if level == 1 {
                    sep = i+start;
                }
            }
            _ => {},
        }
    }

    let left = magnitude(nodes, start+1, sep);
    let right = magnitude(nodes, sep+1, end-1);

    3*left + 2*right
}

fn reduce(mut num: Vec<Node>) -> Vec<Node> {
    loop {
        let mut tmp_num = num.clone();
        let mut level = 0;
        let mut expl = None;
        let mut spl = None;

        for (i, n) in num.iter().enumerate() {
            match n {
                Node::Open => {
                    level += 1;
                    if level > 4 && expl.is_none() {
                        expl = Some(i);
                    }
                },

                Node::Close => level -= 1,
                Node::Num(num) => {
                    if *num >= 10 && spl.is_none() {
                        spl = Some(i);
                    }
                },
                _ => {},
            }
        }

        if let Some(i) = expl {
            explode(&mut tmp_num, i);
            num = tmp_num;
            continue;
        }

        if let Some(i) = spl {
            split(&mut tmp_num, i);
            num = tmp_num;
            continue;
        }

        break;
    }

    num
}

#[aoc_generator(day18)]
fn parse_input(input: &str) -> Vec<Vec<Node>> {
    input.lines()
        .map(parse_line)
        .collect()
}

#[aoc(day18, part1)]
fn part1(input: &[Vec<Node>]) -> isize {
    let mut num = input.get(0).unwrap().to_vec();
    for n2 in input[1..].iter() {
        num.insert(0, Node::Open);
        num.push(Node::Sep);
        num.extend(n2);
        num.push(Node::Close);
        num = reduce(num);
    }

    magnitude(&num, 0, num.len()-1)
}

#[aoc(day18, part2)]
fn part2(input: &[Vec<Node>]) -> isize {
    input.iter().permutations(2)
        .map(|vals| {
            let (x, y) = (vals[0], vals[1]);
            let mut num = vec![Node::Open];
            num.extend(x);
            num.push(Node::Sep);
            num.extend(y);
            num.push(Node::Close);
            num = reduce(num);
            magnitude(&num, 0, num.len()-1)
        }).max().unwrap()
}

#[cfg(test)]
mod test {
    use super::*;

    const INP1: &str = r"[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]";

    #[test]
    fn test_part1() {
        let inp = parse_input(INP1);
        assert_eq!(part1(&inp), 4140);
    }

    #[test]
    fn test_part2() {
        let inp = parse_input(INP1);
        assert_eq!(part2(&inp), 3993);
    }
}
