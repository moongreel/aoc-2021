use std::collections::HashMap;
use regex::Regex;

use std::cmp::{min, max};

struct Line {
    pub start: (usize, usize),
    pub end: (usize, usize),
    pub xmin: usize,
    pub xmax: usize,
    pub ymin: usize,
    pub ymax: usize,
}

impl Line {
    pub fn new(start: (usize, usize), end: (usize, usize)) -> Self {
        Self {
            start,
            end,
            xmin: min(start.0, end.0),
            xmax: max(start.0, end.0),
            ymin: min(start.1, end.1),
            ymax: max(start.1, end.1),
        }
    }

    pub fn is_horizontal(&self) -> bool {
        self.start.1 == self.end.1
    }

    pub fn is_vertical(&self) -> bool {
        self.start.0 == self.end.0
    }
}

#[aoc_generator(day5)]
fn parse_input(input: &str) -> Vec<Line> {
    let pattern = Regex::new(r"^(\d+),(\d+) -> (\d+),(\d+)$").unwrap();

    let mut res = Vec::new();
    for line in input.lines() {
        let caps = pattern.captures(line).unwrap();
        let nums = vec![&caps[1], &caps[2], &caps[3], &caps[4]];
        let nums: Vec<usize> = nums.iter().filter_map(|x| x.parse::<usize>().ok()).collect();
        if nums.len() != 4 {
            continue;
        }
        res.push(Line::new((nums[0], nums[1]),(nums[2], nums[3])));
    }

    res
}

#[aoc(day5, part1)]
fn part1(input: &[Line]) -> usize {
    let mut graph: HashMap<(usize, usize), usize> = HashMap::new();

    for line in input {
        if line.is_vertical() {
            let x = line.start.0;
            for y in line.ymin..line.ymax+1 { 
                let entry = graph.entry((x, y)).or_insert(0);
                *entry += 1;
            }
        } else if line.is_horizontal() {
            let y = line.start.1;
            for x in line.xmin..line.xmax+1 {
                let entry = graph.entry((x, y)).or_insert(0);
                *entry += 1;
            }
        }
    }

    graph.values().filter(|x| **x > 1).count()
}

#[aoc(day5, part2)]
fn part2(input: &[Line]) -> usize {
    let mut graph: HashMap<(usize, usize), usize> = HashMap::new();

    for line in input {
        if line.is_vertical() {
            let x = line.start.0;
            for y in line.ymin..line.ymax+1 {
                let entry = graph.entry((x, y)).or_insert(0);
                *entry += 1;
            }
        } else if line.is_horizontal() {
            let y = line.start.1;
            for x in line.xmin..line.xmax+1 {
                let entry = graph.entry((x, y)).or_insert(0);
                *entry += 1;
            }
        } else {
            let mut y = if line.xmin == line.start.0 {
                line.start.1 as isize
            } else {
                line.end.1 as isize
            };

            let mult = if y > line.ymin as isize { -1 }
            else { 1 };

            for x in line.xmin..line.xmax+1 {
                let entry = graph.entry((x, y as usize)).or_insert(0);
                *entry += 1;
                y += mult;
            }
        }
    }

    graph.values().filter(|x| **x > 1).count()
}

#[cfg(test)]
mod test {
    use super::*;

    const INP1: &str = r"0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2";

    #[test]
    fn test_part1() {
        let inp = parse_input(INP1);
        assert_eq!(part1(&inp), 5);
    }

    #[test]
    fn test_part2() {
        let inp = parse_input(INP1);
        assert_eq!(part2(&inp), 12);
    }
}
