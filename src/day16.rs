use bitreader::BitReader;

struct Packet {
    pub version: u8,
    pub typeid: u8,
    pub data: u64,
    pub packets: Vec<Packet>,
}

impl Packet {
    pub fn new(version: u8, typeid: u8) -> Self {
        Packet { version, typeid, data: 0, packets: Vec::new() }
    }

    pub fn version_sum(&self) -> u64 {
        self.version as u64 + self.packets.iter().map(|x| x.version_sum()).sum::<u64>()
    }

    pub fn operate(&self) -> u64 {
        match self.typeid {
            0 => self.packets.iter().map(|x| x.operate()).sum(),
            1 => self.packets.iter().map(|x| x.operate()).product(),
            2 => self.packets.iter().map(|x| x.operate()).min().unwrap(),
            3 => self.packets.iter().map(|x| x.operate()).max().unwrap(),
            4 => self.data,
            5 => {
                if self.packets[0].operate() > self.packets[1].operate() { 1 }
                else { 0 }
            },
            6 => {
                if self.packets[0].operate() < self.packets[1].operate() { 1 }
                else { 0 }
            },
            7 => {
                if self.packets[0].operate() == self.packets[1].operate() { 1 }
                else { 0 }
            },
            _ => panic!("Invalid packet type ID"),
        }
    }
}

#[aoc_generator(day16)]
fn parse_input(input: &str) -> Vec<u8> {
    let input: Vec<_> = input
        .chars()
        .filter_map(|x| x.to_digit(16))
        .map(|x| x as u8)
        .collect();

    let mut bytes: Vec<_> = (0..input.len()-1).step_by(2).map(|i| {
        (input[i] << 4) | input[i+1]
    }).collect();

    if input.len() % 2 == 1 {
        bytes.push(input.last().unwrap() << 4);
    }

    bytes
}

fn read_packet(rdr: &mut BitReader) -> bitreader::Result<Packet> {
    let ver = rdr.read_u8(3)?;
    let typeid = rdr.read_u8(3)?;
    let pkt = Packet::new(ver, typeid);
    match typeid {
        4 => read_literal(rdr, pkt),
        _ => read_operator(rdr, pkt),
    }
}

fn read_literal(rdr: &mut BitReader, mut pkt: Packet) -> bitreader::Result<Packet> {
    let mut data = Vec::new();
    loop {
        let flag = rdr.read_u8(1)?;
        data.push(rdr.read_u8(4)? as u64);
        if flag == 0 { break; }
    }

    data.reverse();
    pkt.data = data.iter().enumerate().fold(0, |acc, (i, val)| {
        acc | (val << (i * 4))
    });

    Ok(pkt)
}

fn read_operator(rdr: &mut BitReader, mut pkt: Packet) -> bitreader::Result<Packet> {
    let lflag = rdr.read_u8(1)?;
    let mut packets = Vec::new();
    if lflag == 0 {
        let length = rdr.read_u16(15)?;
        let nbytes = length / 8;
        let mut bytes = vec![0u8;nbytes as usize];
        rdr.read_u8_slice(&mut bytes)?;
        if length % 8 != 0 {
            let nbits = length - nbytes*8;
            let val = rdr.read_u8(nbits as u8)?;
            bytes.push(val << (8 - nbits));
        }
        let mut tmp_rdr = BitReader::new(&bytes);
        let end = tmp_rdr.remaining() - length as u64;

        while let Ok(packet) = read_packet(&mut tmp_rdr) {
            if end > tmp_rdr.remaining() {
                break;
            }
            packets.push(packet);
        }
    } else {
        let length = rdr.read_u16(11)?;
        for _ in 0..length {
            packets.push(read_packet(rdr)?);
        }
    }

    pkt.packets = packets;

    Ok(pkt)
}

#[aoc(day16, part1)]
fn part1(input: &[u8]) -> u64 {
    let mut rdr = BitReader::new(input);

    read_packet(&mut rdr).unwrap().version_sum()
}

#[aoc(day16, part2)]
fn part2(input: &[u8]) -> u64 {
    let mut rdr = BitReader::new(input);

    read_packet(&mut rdr).unwrap().operate()
}

#[cfg(test)]
mod test {
    use super::*;

    const INP1: &str = r"8A004A801A8002F478";
    const INP2: &str = r"620080001611562C8802118E34";
    const INP3: &str = r"C0015000016115A2E0802F182340";
    const INP4: &str = r"A0016C880162017C3686B18A3D4780";

    const INP5: &str = r"C200B40A82";
    const INP6: &str = r"04005AC33890";
    const INP7: &str = r"880086C3E88112";
    const INP8: &str = r"CE00C43D881120";
    const INP9: &str = r"D8005AC2A8F0";
    const INP10: &str = r"F600BC2D8F";
    const INP11: &str = r"9C005AC2F8F0";
    const INP12: &str = r"9C0141080250320F1802104A08";

    #[test]
    fn test_part1() {
        let inp = parse_input(INP1);
        assert_eq!(part1(&inp), 16);
        let inp = parse_input(INP2);
        assert_eq!(part1(&inp), 12);
        let inp = parse_input(INP3);
        assert_eq!(part1(&inp), 23);
        let inp = parse_input(INP4);
        assert_eq!(part1(&inp), 31);
    }

    #[test]
    fn test_part2() {
        let inp = parse_input(INP5);
        assert_eq!(part2(&inp), 3);
        let inp = parse_input(INP6);
        assert_eq!(part2(&inp), 54);
        let inp = parse_input(INP7);
        assert_eq!(part2(&inp), 7);
        let inp = parse_input(INP8);
        assert_eq!(part2(&inp), 9);
        let inp = parse_input(INP9);
        assert_eq!(part2(&inp), 1);
        let inp = parse_input(INP10);
        assert_eq!(part2(&inp), 0);
        let inp = parse_input(INP11);
        assert_eq!(part2(&inp), 0);
        let inp = parse_input(INP12);
        assert_eq!(part2(&inp), 1);
    }
}
