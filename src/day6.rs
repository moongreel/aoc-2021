#[derive(Debug, Clone)]
struct Fish {
    pub timer: isize,
    pub amt: usize,
}

impl Fish {
    pub fn new(timer: isize, amt: usize) -> Self {
        Self { timer, amt }
    }

    pub fn tick(&mut self) -> bool {
        self.timer -= 1;
        if self.timer < 0 {
            self.timer = 6;
            return true;
        }
        false
    }
}

#[aoc_generator(day6)]
fn parse_input(input: &str) -> Vec<Fish> {
    input.split(',').map(|x| Fish::new(x.parse::<isize>().unwrap(), 1)).collect()
}

#[aoc(day6, part1)]
fn part1(input: &[Fish]) -> usize {
    let mut fishes: Vec<_> = input.to_vec();

    for _day in 0..80 {
        let mut added = 0;
        for fish in fishes.iter_mut() {
            if fish.tick() {
                added += fish.amt;
            }
        }
        if added > 0 {
            fishes.push(Fish::new(8, added));
        }
    }

    fishes.iter().map(|x| x.amt).sum()
}

#[aoc(day6, part2)]
fn part2(input: &[Fish]) -> usize {
    let mut fishes: Vec<_> = input.to_vec();

    for _day in 0..256 {
        let mut added = 0;
        for fish in fishes.iter_mut() {
            if fish.tick() {
                added += fish.amt;
            }
        }
        if added > 0 {
            fishes.push(Fish::new(8, added));
        }
    }

    fishes.iter().map(|x| x.amt).sum()
}

#[cfg(test)]
mod test {
    use super::*;

    const INP1: &str = r"3,4,3,1,2";

    #[test]
    fn test_part1() {
        let inp = parse_input(INP1);
        assert_eq!(part1(&inp), 5934);
    }

    #[test]
    fn test_part2() {
        let inp = parse_input(INP1);
        assert_eq!(part2(&inp), 26984457539);
    }
}
