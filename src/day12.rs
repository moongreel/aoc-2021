use std::collections::HashMap;

#[derive(PartialEq,Hash,Eq,Clone,Debug)]
enum Node {
    Start,
    End,
    Big(String),
    Small(String),
}

impl Node {
    pub fn new(name: &str) -> Self {
        let upper = name.to_uppercase() == name;
        match name {
            "start" => Node::Start,
            "end" => Node::End,
            _ => {
                if upper { Node::Big(name.to_string()) }
                else { Node::Small(name.to_string()) }
            }
        }
    }

    pub fn is_small(&self) -> bool {
        matches!(self, Node::Small(_))
    }
}


fn num_paths(node: &Node, smalls: &[Node], graph: &HashMap<Node, Vec<Node>>) -> usize {
    let mut smalls = smalls.to_vec();
    if node.is_small() {
        if smalls.contains(node) { return 0; }
        else { smalls.push(node.clone()); }
    }

    if node == &Node::End {
        return 1;
    }

    let nodes = graph.get(node).unwrap();
    nodes.iter().map(|n| num_paths(n, &smalls, graph)).sum()
}

fn num_paths_mod(node: &Node, smalls: &[Node], graph: &HashMap<Node, Vec<Node>>, mut used: bool) -> usize {
    let mut smalls = smalls.to_vec();
    if node.is_small() {
        if smalls.contains(node) {
            if used { return 0; }
            else { used = true; }
        } else { smalls.push(node.clone()); }
    }

    if node == &Node::End {
        return 1;
    }

    let nodes = graph.get(node).unwrap();
    nodes.iter().map(|n| num_paths_mod(n, &smalls, graph, used)).sum()
}

#[aoc_generator(day12)]
fn parse_input(input: &str) -> HashMap<Node, Vec<Node>> {
    let mut edges = HashMap::new();

    for line in input.lines() {
        let (n1, n2) = line.split_once('-').unwrap();
        let n1 = Node::new(n1);
        let n2 = Node::new(n2);

        if n2 != Node::Start {
            let e1 = edges.entry(n1.clone()).or_insert_with(Vec::new);
            e1.push(n2.clone());
        }

        if n1 != Node::Start {
            let e2 = edges.entry(n2).or_insert_with(Vec::new);
            e2.push(n1);
        }
    }

    edges
}

#[aoc(day12, part1)]
fn part1(input: &HashMap<Node, Vec<Node>>) -> usize {
    num_paths(&Node::Start, &Vec::new(), input)
}

#[aoc(day12, part2)]
fn part2(input: &HashMap<Node, Vec<Node>>) -> usize {
    num_paths_mod(&Node::Start, &Vec::new(), input, false)
}

#[cfg(test)]
mod test {
    use super::*;

    const INP1: &str = r"start-A
start-b
A-c
A-b
b-d
A-end
b-end";

    #[test]
    fn test_part1() {
        let inp = parse_input(INP1);
        assert_eq!(part1(&inp), 10);
    }

    #[test]
    fn test_part2() {
        let inp = parse_input(INP1);
        assert_eq!(part2(&inp), 36);
    }
}
