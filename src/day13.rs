use std::collections::HashSet;

enum Axis { X, Y }

struct Fold {
    line: isize,
    axis: Axis,
}

fn fold(val: isize, line: isize) -> isize {
    if val > line { line-(val-line) } else { val }
}

impl Fold {
    pub fn new(axis: &str, line: isize) -> Self {
        match axis {
            "x" => Fold { line, axis: Axis::X },
            "y" => Fold { line, axis: Axis::Y },
            _ => panic!(),
        }
    }

    pub fn fold(&self, pt: &(isize, isize)) -> (isize, isize) {
        match self.axis {
            Axis::X => (fold(pt.0, self.line), pt.1),
            Axis::Y => (pt.0, fold(pt.1, self.line)),
        }
    }
}

#[aoc_generator(day13)]
fn parse_input(input: &str) -> (HashSet<(isize, isize)>, Vec<Fold>) {
    let (dots, folds) = input.split_once("\n\n").unwrap();

    let dots = dots.lines().map(|l| {
        let (x, y) = l.split_once(',').unwrap();
        let x = x.parse::<isize>().unwrap();
        let y = y.parse::<isize>().unwrap();
        (x, y)
    }).collect();

    let folds = folds.lines().map(|l| {
        let ins: Vec<&str> = l.split(' ').collect();
        let (axis, amt) = ins[2].split_once('=').unwrap();
        let amt = amt.parse::<isize>().unwrap();
        Fold::new(axis, amt)
    }).collect();

    (dots, folds)
}

#[aoc(day13, part1)]
fn part1(input: &(HashSet<(isize, isize)>, Vec<Fold>)) -> usize {
    let (paper, folds) = input;
    let fold = folds.get(0).unwrap();
    paper.iter().map(|x| fold.fold(x)).collect::<HashSet<(isize, isize)>>().len()
}

#[aoc(day13, part2)]
fn part2(input: &(HashSet<(isize, isize)>, Vec<Fold>)) -> String {
    let (paper, folds) = input;
    let mut paper = paper.clone();

    for fold in folds {
        paper = paper.iter().map(|x| fold.fold(x)).collect()
    }

    let minx = *paper.iter().map(|(x, _)| x).min().unwrap();
    let maxx = *paper.iter().map(|(x, _)| x).max().unwrap();
    let miny = *paper.iter().map(|(_, y)| y).min().unwrap();
    let maxy = *paper.iter().map(|(_, y)| y).max().unwrap();

    format!("\n{}", (miny..=maxy).into_iter().map(|y| {
        (minx..=maxx).into_iter().map(|x| {
            match paper.get(&(x, y)) {
                Some(_) => "XX",
                _ => "  ",
            }
        }).collect::<String>()
    }).collect::<Vec<String>>().join("\n"))
}

#[cfg(test)]
mod test {
    use super::*;

    const INP1: &str = r"6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5";

    #[test]
    fn test_part1() {
        let inp = parse_input(INP1);
        assert_eq!(part1(&inp), 17);
    }

    #[test]
    fn test_part2() {
        let inp = parse_input(INP1);
        assert_eq!(part2(&inp),r"
XXXXXXXXXX
XX      XX
XX      XX
XX      XX
XXXXXXXXXX"
        );
    }
}
