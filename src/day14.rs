use std::collections::HashMap;

struct Rule {
    pub pair: [u8;2],
    pub out: u8,
}

#[aoc_generator(day14)]
fn parse_input(input: &str) -> (Vec<u8>, Vec<Rule>) {
    let (start, rules) = input.split_once("\n\n").unwrap();

    let rules = rules.lines().map(|x| {
        let (pair, out) = x.split_once(" -> ").unwrap();
        let pair: Vec<u8> = pair.chars().map(|x| x as u8).collect();
        Rule {
            pair: [pair[0], pair[1]],
            out: out.chars().next().unwrap() as u8,
        }
    }).collect();

    (start.chars().map(|x| x as u8).collect(), rules)
}

#[aoc(day14, part1)]
fn part1(input: &(Vec<u8>, Vec<Rule>)) -> usize {
    let (start, rules) = input;
    let mut start = start.clone();

    for _ in 0..10 {
        let mut inserts = Vec::new();
        for i in 0..start.len()-1 {
            let pair: [u8;2] = [start[i], start[i+1]];
            for rule in rules {
                if pair == rule.pair {
                    inserts.push((i+1, rule.out));
                }
            }
        }

        for (i, (idx, ins)) in inserts.iter().enumerate() {
            start.insert(i+idx, *ins);
        }
    }

    let mut counts: HashMap<u8, usize> = HashMap::new();
    for c in start {
        let e = counts.entry(c).or_insert(0);
        *e += 1;
    }

    let max = counts.values().max().unwrap();
    let min = counts.values().min().unwrap();

    max - min
}

#[aoc(day14, part2)]
fn part2(input: &(Vec<u8>, Vec<Rule>)) -> u64 {
    let (start, rules) = input;
    let mut pairs: HashMap<[u8;2], u64> = HashMap::new();

    for i in 0..start.len()-1 {
        let pair = [start[i], start[i+1]];
        let e = pairs.entry(pair).or_insert(0);
        *e += 1;
    }


    for _ in 0..40 {
        let mut tmp_pairs = pairs.clone();
        for pair in pairs.keys() {
            for rule in rules {
                if &rule.pair == pair {
                    let amt = pairs.get(pair).unwrap();
                    *tmp_pairs.get_mut(pair).unwrap() -= amt;
                    for p in [[pair[0],rule.out], [rule.out,pair[1]]] {
                        let e = tmp_pairs.entry(p).or_insert(0);
                        *e += amt;
                    }
                }
            }
        }
        pairs = tmp_pairs;
    }

    let mut counts: HashMap<u8, u64> = HashMap::new();
    for (pair, amt) in pairs {
        for c in pair {
            let e = counts.entry(c).or_insert(0);
            *e += amt;
        }
    }

    let max = (*counts.values().max().unwrap() + 1) / 2;
    let min = (*counts.values().min().unwrap() + 1) / 2;
    
    max - min
}

#[cfg(test)]
mod test {
    use super::*;

    const INP1: &str = r"NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C";

    #[test]
    fn test_part1() {
        let inp = parse_input(INP1);
        assert_eq!(part1(&inp), 1588);
    }

    #[test]
    fn test_part2() {
        let inp = parse_input(INP1);
        assert_eq!(part2(&inp), 2188189693529);
    }
}
