#[aoc_generator(day1)]
fn parse_input(input: &str) -> Vec<isize> {
    input
        .lines()
        .filter_map(|x| x.parse::<isize>().ok())
        .collect()
}

#[aoc(day1, part1)]
fn part1(lines: &[isize]) -> isize {
    (1..lines.len()).into_iter()
        .fold(0, |count, i| {
            if lines[i] > lines[i-1] { count + 1 }
            else { count }
        }
    )
}

#[aoc(day1, part2)]
fn part2(lines: &[isize]) -> isize {
    (0..lines.len()-3).into_iter()
        .fold(0, |count, i| {
            let a: isize = lines[i..i+3].iter().sum();
            let b: isize = lines[i+1..i+4].iter().sum();
            if a < b { count + 1 }
            else { count }
        })
}

#[cfg(test)]
mod test {
    use super::*;

    const INP1: &str = r"199
200
208
210
200
207
240
269
260
263";

    #[test]
    fn test_part1() {
        let inp = parse_input(INP1);
        assert_eq!(part1(&inp), 7);
    }

    #[test]
    fn test_part2() {
        let inp = parse_input(INP1);
        assert_eq!(part2(&inp), 5);
    }
}
