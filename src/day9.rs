use std::collections::HashMap;

#[aoc_generator(day9)]
fn parse_input(input: &str) -> Vec<Vec<u32>> {
    input
        .lines()
        .map(|x| x.chars().filter_map(|y| y.to_digit(10)).collect())
        .collect()
}

#[aoc(day9, part1)]
fn part1(input: &[Vec<u32>]) -> u32 {
    let rows = input.len();
    let cols = input[0].len();
    let mut risks = 0;

    for (i, row) in input.iter().enumerate() {
        for (j, val) in row.iter().enumerate() {
            let above = if i == 0 { 10 }
            else { input[i-1][j] };

            let left = if j == 0 { 10 }
            else { input[i][j-1] };

            let right = if j == cols - 1  { 10 }
            else { input[i][j+1] };

            let below = if i == rows - 1 { 10 }
            else { input[i+1][j] };

            let val = *val;
            if val < above && val < left && val < right && val < below {
                risks += val + 1;
            }
        }
    }

    risks
}

#[aoc(day9, part2)]
fn part2(input: &[Vec<u32>]) -> usize {
    let rows = input.len();
    let cols = input[0].len();
    let mut input_mut = input.to_vec();
    let mut basin_map: HashMap<(isize, isize), usize> = HashMap::new();
    let mut basins: Vec<usize> = Vec::new();
    let mut end;

    loop {
        end = true;
        let input = input_mut.clone();
        for (i, row) in input_mut.iter_mut().enumerate() {
            for (j, val) in row.iter_mut().enumerate() {
                if *val == 9 { continue; }
                let above = if i == 0 { 9 }
                else { input[i-1][j] };

                let left = if j == 0 { 9 }
                else { input[i][j-1] };

                let right = if j == cols - 1  { 9 }
                else { input[i][j+1] };

                let below = if i == rows - 1 { 9 }
                else { input[i+1][j] };

                let cmp = *val;
                if cmp <= above && cmp <= left && cmp <= right && cmp <= below {
                    let ii = i as isize;
                    let jj = j as isize;

                    let idx = if basin_map.contains_key(&(ii-1, jj)) {
                        *basin_map.get(&(ii-1, jj)).unwrap()
                    } else if basin_map.contains_key(&(ii, jj-1)) {
                        *basin_map.get(&(ii, jj-1)).unwrap()
                    } else if basin_map.contains_key(&(ii+1, jj)) {
                        *basin_map.get(&(ii+1, jj)).unwrap()
                    } else if basin_map.contains_key(&(ii, jj+1)) {
                        *basin_map.get(&(ii, jj+1)).unwrap()
                    } else {
                        basins.push(0);
                        basins.len() - 1
                    };
                    basin_map.insert((ii, jj), idx);
                    basins[idx] += 1;
                    end = false;
                    *val = 9;
                }
            }
        }

        if end { break; }
    }

    basins.sort_unstable();
    basins.reverse();

    basins[0] * basins[1] * basins[2]
}

#[cfg(test)]
mod test {
    use super::*;

    const INP1: &str = r"2199943210
3987894921
9856789892
8767896789
9899965678";

    #[test]
    fn test_part1() {
        let inp = parse_input(INP1);
        assert_eq!(part1(&inp), 15);
    }

    #[test]
    fn test_part2() {
        let inp = parse_input(INP1);
        assert_eq!(part2(&inp), 1134);
    }
}
